# Godot PostgreSQL Model Demo

This repository demonstrates usage of Marzin's PostgreSQL Client for Godot, along with basic usage of a custom Model used to generate SQL statements based on inputs, similar to an ORM. There is also a sample Controller used to simplify database record access, and provide a more user-friendly API to read and write database records.

# How To Use

* Set up AutoLoad (or manage loading yourself) for `Util`, `Database`, `Models` and `Controllers` GDScripts, and provide your database credentials and configuration in `Database`.
* You can hook into `Database`'s signal, `database_connected`, to wait on a valid database connection.
* Populate tables and records in your database, and apply the pattern established in `Models/Accounts.gd` and `Controllers/Accounts.gd` to implement your own functionality.

