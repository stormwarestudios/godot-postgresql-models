extends Node


var _models:Dictionary = {}


func _init() -> void:
	load_models()


func load_models():
	var model_names = []
	var files = Util.list_files_in_directory("res://Lib/Models/")
	for file in files:
		_models[file.get_basename()] = load("res://Lib/Models/%s" % [ file ]).new()


func get_model(name:String):
	if _models.has(name):
		return _models[name]
	print("Model %s not found" % [ name ])
	return null

