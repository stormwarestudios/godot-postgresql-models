extends Node
class_name BaseModel


var table_name:String = ""
var auto_create:Dictionary = {}
var auto_update:Dictionary = {}
var _editor_client


func create(criteria: Dictionary = {}, dry = false) -> Dictionary:
	for k in auto_create.keys():
		if !criteria.has(k):
			var method = auto_create[k]
			criteria[k] = call(method)
	
	var keys = criteria.keys()
	var values = []
	
	# Iterate like this so the key/value ordering is correct
	for key in keys:
		var value = criteria[key]
		if value == null:
			value = ""
		values.append("'%s'" % [ value ])
	
	# Convert column-names to double-quoted for case sensitivity
	for i in range(keys.size()):
		keys[i] = '"%s"' % [ keys[i] ]
	
	var columnNames = PoolStringArray(keys).join(", ")
	var columnValues = PoolStringArray(values).join(", ")
	
	var sql = 'INSERT INTO "%s" (%s) VALUES (%s) RETURNING *' % [ table_name, columnNames, columnValues ]
	print(sql, "debug")
	
	if dry:
		return { "sql": sql }
	
	var rows = _get_database_client().execute(sql)
	var result = _extract_records(rows)
	if result.size() == 1:
		return result[0]
	return {}


func try_cache(id):
	pass


func read(criteria: Dictionary = {}, limit = 100, offset:int = 0, order:Dictionary = {}, return_columns = null) -> Array:
#	var cache = Database.get_memory_store()
#	if criteria.has("id"):
#		if cache:
#			var cache_key = "%s:%s" % [ table_name, criteria.id ]
#			if cache.has_item(cache_key):
#				return [cache.get_item_data(cache_key)]

	var columns = _build_columns(return_columns)
	var where = _build_where(criteria)
	var pagination = _build_pagination(limit, offset)
	var order_by = _build_order_by(order)
	
	var sql = 'SELECT %s FROM "%s" %s %s %s' % [ columns, table_name, where, order_by, pagination ]
	print(sql, "debug")
	
	var rows = _get_database_client().execute(sql)
	var records = _extract_records(rows)
#	if cache:
#		for record in records:
#			var cache_key = "%s:%s" % [ table_name, record.id ]
#			cache.set_item(cache_key, record)
	return records


func read_v2(options) -> Array:
	var criteria = {}
	if options.has("criteria"):
		criteria = options["criteria"]
	
	var limit = 100
	if options.has("limit"):
		limit = options["limit"]

	var offset = 0
	if options.has("offset"):
		offset = options["offset"]

	var order = null
	if options.has("order"):
		order = options["order"]
	
	var columns = "*"
	if options.has("columns"):
		print("BaseModel::read_v2() 'columns' not implemented yet")
	
	var where = _build_where(criteria)
	var pagination = _build_pagination(limit, offset)
	var order_by = _build_order_by(order)
	
	var sql = 'SELECT * FROM "%s" %s %s %s' % [ table_name, where, order_by, pagination ]
	print(sql, "debug")
	
	var rows = _get_database_client().execute(sql)
	return _extract_records(rows)


func read_by_id(id: String):
	var result = read({ "id": id }, 1, 0)
	if result.size() == 1:
		return result[0]
	return null


func update(criteria: Dictionary = {}, what: Dictionary = {}):
	for k in auto_update.keys():
		if !what.has(k):
			var method = auto_update[k]
			what[k] = call(method)
	
	var where = _build_where(criteria)
	var set = _build_set(what)
	
	var sql = 'UPDATE "%s" %s %s RETURNING *' % [ table_name, set, where ]
	print(sql, "debug")
	
	var rows = _get_database_client().execute(sql)
	return _extract_records(rows)


func delete(criteria: Dictionary = {}) -> Array:
	var where = _build_where(criteria)
	var sql = 'DELETE FROM "%s" %s RETURNING *' % [ table_name, where ]
	var rows = _get_database_client().execute(sql)
	return _extract_records(rows)


func count(criteria: Dictionary = {}) -> int:
	var where = _build_where(criteria)
	var sql = 'SELECT COUNT(*) FROM "%s" %s' % [ table_name, where ]
	print(sql, "debug")
	var rows = _get_database_client().execute(sql)
	var result = _extract_records(rows)
	return result[0].count


func _get_database_client():
	if Engine.is_editor_hint():
		if _editor_client == null:
			var _database = load("res://Database/Database.gd").new()
			_database
			pass
		
		return _editor_client
	else:
		return Database.get_client()


func _build_columns(columns) -> String:
	if columns == null:
		return "*"
	elif typeof(columns) == TYPE_ARRAY:
		# TODO: return columns
		return "*"
	elif typeof(columns) == TYPE_DICTIONARY:
		# TODO: return aliased columns
		return "*"
	else:
		return "*"


func _build_where(criteria: Dictionary = {}) -> String:
	var wheres = []
	for key in criteria.keys():
		var value = criteria[key]
		if value == null:
			wheres.append("\"%s\" IS NULL" % [ key ])
		else:
			wheres.append("\"%s\" = '%s'" % [ key, value ])
	if wheres.size() == 0:
		return ""
	return "WHERE %s" % [ PoolStringArray(wheres).join(" AND ") ]


func _build_set(set:Dictionary = {}) -> String:
	var sets = []
	for key in set.keys():
		var value = set[key]
		if value == null:
			value = ""
		sets.append("\"%s\" = '%s'" % [ key, value ])
	return "SET %s" % [ PoolStringArray(sets).join(", ") ]


func _build_pagination(limit = 100, offset:int = 0) -> String:
	return "LIMIT %s OFFSET %s" % [ limit, offset ]


func _build_order_by(order_info:Dictionary = {}) -> String:
	var orders = []
	
	for key in order_info.keys():
		orders.append('"%s" %s' % [ key, order_info[key] ])
	
	if orders.size() > 0:
		return "ORDER BY %s" % [ PoolStringArray(orders).join(", ") ]
	
	return ""


func _extract_records(query_results) -> Array:
	var result = []
	for qr in query_results:
		var field_count = qr.number_of_fields_in_a_row
		var desc = qr.row_description
		var rows = qr.data_row
		for row in rows:
			result.append(_extract_record(row, desc, field_count))
	return result


func _extract_record(row, desc, field_count):
	var record = {}
	for i in field_count:
		var key = desc[i]["field_name"]
		var value = row[i]
		if typeof(value) == TYPE_RAW_ARRAY:
			value = value.get_string_from_ascii()
		record[key] = value
	return record


func _generate_datetime() -> String:
	var now = OS.get_datetime(true) # true -> UTC
	return "%04d-%02d-%02dT%02d:%02d:%02dZ" % [now.year, now.month, now.day, now.hour, now.minute, now.second]
