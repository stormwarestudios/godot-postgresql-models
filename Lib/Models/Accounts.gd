extends "res://Lib/BaseModel.gd"


func _init() -> void:
	table_name = "Accounts"
	auto_create = {
		"id": "_generate_uuid",
		"createdAt": "_generate_datetime",
		"updatedAt": "_generate_datetime"
	}
	auto_update = {
		"updatedAt": "_generate_datetime"
	}
