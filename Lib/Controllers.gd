extends Node


var _controllers:Dictionary = {}


func _init() -> void:
	load_models()


func load_models():
	var model_names = []
	var files = Util.list_files_in_directory("res://Lib/Controllers/")
	for file in files:
		_controllers[file.get_basename()] = load("res://Lib/Controllers/%s" % [ file ]).new()


func get_controller(name:String):
	if _controllers.has(name):
		return _controllers[name]
	print("Controller %s not found" % [ name ])
	return null
