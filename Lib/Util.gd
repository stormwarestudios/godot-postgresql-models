extends Node


func list_files_in_directory(path):
	# https://godotengine.org/qa/5175/how-to-get-all-the-files-inside-a-folder?show=5176#a5176
	var files = []
	var dir = Directory.new()
	dir.open(path)
	dir.list_dir_begin()
	while true:
		var file = dir.get_next()
		if file == "":
			break
		elif not file.begins_with("."):
			files.append(file)
	dir.list_dir_end()
	return files


func env_get(key:String, default = ""):
	if OS.has_environment(key):
		return OS.get_environment(key)
	else:
		return default
