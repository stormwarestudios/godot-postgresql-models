extends Node

signal database_connected
signal database_error
signal database_closed

var PostgreSQLClient = load("res://Lib/PostgreSQLClient.gd")
var database = PostgreSQLClient.new()

const USERNAME = "postgres"
const PASSWORD = "postgres"
const HOST = "127.0.0.1"
const PORT = 5432
const DATABASE = "postgres"
const APPLICATION = "godot"


func _ready():
	var _error = database.connect("connection_established", self, "_on_connection_established")
	_error = database.connect("authentication_error", self, "_authentication_error")
	_error = database.connect("connection_closed", self, "_close")
	
	var url = get_connection_string()
	print("Connecting to %s" % [ get_connection_string() ])
	
	# Connection to the database
	_error = database.connect_to_host(url, false, 5)
	yield(database, "connection_established")


func _physics_process(_delta: float) -> void:
	database.poll()


func get_connection_string() -> String:
	return "postgresql://%s:%s@%s:%d/%s" % [ get_username(), get_password(), get_hostname(), get_port(), get_database() ]


func get_connection_string_safe() -> String:
	return "postgresql://%s:*****@%s:%d/%s" % [ get_username(), get_hostname(), get_port(), get_database() ]


func get_hostname() -> String:
	return Util.env_get("DB_HOSTNAME", HOST)


func get_port() -> int:
	return Util.env_get("DB_PORT", PORT)


func get_username() -> String:
	return Util.env_get("DB_USERNAME", USERNAME)


func get_database() -> String:
	return Util.env_get("DB_DBNAME", DATABASE)


func get_password() -> String:
	return Util.env_get("DB_PASSWORD", PASSWORD)


func get_application() -> String:
	return Util.env_get("DB_APPLICATION", APPLICATION)


func _on_connection_established() -> void:
	emit_signal("database_connected")
	print("Database::connection_established()", "debug")


func _on_connection_closed() -> void:
	emit_signal("database_closed")
	print("Database::connection_closed()", "error")


func _on_authentication_error(error_object: Dictionary) -> void:
	emit_signal("database_error")
	print("Database::authentication_error()", "error", error_object["message"])


func get_client():
	return database


func _exit_tree() -> void:
	database.close()


func _extract_records(query_results) -> Array:
	var result = []
	for qr in query_results:
		var field_count = qr.number_of_fields_in_a_row
		var desc = qr.row_description
		var rows = qr.data_row
		for row in rows:
			result.append(_extract_record(row, desc, field_count))
	return result


func _extract_record(row, desc, field_count):
	var record = {}
	for i in field_count:
		var key = desc[i]["field_name"]
		var value = row[i]
		if typeof(value) == TYPE_RAW_ARRAY:
			value = value.get_string_from_ascii()
		record[key] = value
	return record


func executeRawSQL(sql:String):
	print(sql, "debug")
	var rows = database.execute(sql)
	return _extract_records(rows)

